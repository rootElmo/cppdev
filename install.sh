#!/bin/bash

mkdir -p /home/${USER}/skeletons
cp ./README.md /home/${USER}/skeletons/README.md
cp ./CMakeLists.txt /home/${USER}/skeletons/CMakeLists.txt
