# Project name

Short project description

## Table of Contents

 - [Requirements](#requirements)
 - [Usage](#usage)
 - [Maintainers](#maintainers)
 - [Contributing](#contributing)
 - [License](#license)

## Requirements

List of requirements

## Usage

Instructions on how to use the program/project. Possible list of inputs/outputs/operations for the program/project.

![prog_out001](./imgs/word_calc001.png)

## Maintainers

[Elmo Rohula @rootElmo](https://gitlab.com/rootElmo)

## Contributing

List of contributors

## License

License description
